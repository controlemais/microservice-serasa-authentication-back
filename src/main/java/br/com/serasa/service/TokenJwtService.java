package br.com.serasa.service;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import br.com.serasa.model.Usuario;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class TokenJwtService {

	public String recuperarToken(Usuario usuario) {
		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

		Map<String, Object> claims = new HashMap<String, Object>();

		claims.put("login", usuario.getLogin());
		claims.put("nome", usuario.getNome());

		String jws = Jwts.builder()
				.setSubject(usuario.getUsuarioId().toString())
				.addClaims(claims)
				.signWith(key).compact();

		return jws;
	}

}
