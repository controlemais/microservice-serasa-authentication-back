package br.com.serasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceSerasaAuthenticationBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceSerasaAuthenticationBackApplication.class, args);
	}

}
