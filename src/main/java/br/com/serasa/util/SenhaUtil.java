package br.com.serasa.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.serasa.config.SecurityConstants;

public class SenhaUtil {
	public static String encodePassword(String password) {
		String encoded = new BCryptPasswordEncoder().encode(password).concat(SecurityConstants.KEY_PASSWORD);
		return encoded;
	}
}
