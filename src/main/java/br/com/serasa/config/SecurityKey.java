package br.com.serasa.config;

import javax.crypto.SecretKey;

import io.jsonwebtoken.security.Keys;

public class SecurityKey {
	private SecretKey _key;
	private static SecurityKey instance;

	public static SecretKey recuperarKey() {
		if (instance == null) {
			instance = new SecurityKey();
		}

		return instance._key;
	}

	private SecurityKey() {
		byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();

		this._key = Keys.hmacShaKeyFor(signingKey);
	}
}
